import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private db: AngularFireDatabase) { }

  create(product) {
    return this.db.list('/product').push(product);
  }
  getAll() {
    // this.courses$ = this.courses.snapshotChanges();
    return this.db.list('/product').snapshotChanges()
    .map(resp =>  resp.map( elem => {
      return {
        key: elem.key, ...elem.payload.val()
      }
  }))
  }

  get(productId) {
    return this.db.object('/product/' + productId)
  }

  update(productId, product) {
    return this.db.object('/product/' + productId).update(product)
  }

  delete(productId) {
    return this.db.object('/product/' + productId).remove();
  }

}
