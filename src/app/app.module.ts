import { CategoryService } from './services/category.service';
import { ProductFormComponent } from './admin/product-form/product-form.component';
import { AdminAuthGuardService } from './services/admin-auth-guard.service';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';
import { AdminOrdersComponent } from './admin/admin-orders/admin-orders.component';
import { AdminProductsComponent } from './admin/admin-products/admin-products.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DataTableModule } from "angular-6-datatable";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './home/home.component';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { ProductsComponent } from './products/products.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { OrderSuccessComponent } from './order-success/order-success.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { UserService } from './services/user.service';
import { FormsModule } from '@angular/forms';
import { ProductService } from './services/product.service';
import { CustomFormsModule } from 'ng2-validation';

@NgModule({
   declarations: [
      AppComponent,
      HomeComponent,
      NavbarComponent,
      ProductsComponent,
      ShoppingCartComponent,
      CheckOutComponent,
      OrderSuccessComponent,
      MyOrdersComponent,
      LoginComponent,
      AdminProductsComponent,
      AdminOrdersComponent,
      ProductFormComponent
   ],
   imports: [
      DataTableModule,
      CustomFormsModule,
      FormsModule,
      NgbModule.forRoot(),
      BrowserModule,
      AppRoutingModule,
      AngularFireModule.initializeApp(environment.firebase),
      AngularFireDatabaseModule,
      AngularFireAuthModule,
      RouterModule.forRoot([
         { path: '', component: HomeComponent },
         { path: 'login', component: LoginComponent },
         { path: 'products', component: ProductsComponent },
         { path: 'shopping-cart', component: ShoppingCartComponent },
         
         { path: 'check-out', component: CheckOutComponent, canActivate: [AuthGuardService]},
         { path: 'order-success', component: OrderSuccessComponent, canActivate: [AuthGuardService] },
         { path: 'my/orders', component: MyOrdersComponent, canActivate: [AuthGuardService] },

         // Admin Routes
         { path: 'admin/products/new', component: ProductFormComponent, canActivate: [AuthGuardService, AdminAuthGuardService] },
         { path: 'admin/products/:id', component: ProductFormComponent, canActivate: [AuthGuardService, AdminAuthGuardService] },
         { path: 'admin/products', component: AdminProductsComponent, canActivate: [AuthGuardService, AdminAuthGuardService] },
         { path: 'admin/orders', component: AdminOrdersComponent, canActivate: [AuthGuardService, AdminAuthGuardService] },
      ])
   ],
   providers: [
      AuthService,
      AuthGuardService,
      AdminAuthGuardService,
      UserService,
      CategoryService,
      ProductService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
